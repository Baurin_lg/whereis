from __future__ import unicode_literals


from django.db import models
from django.utils import timezone
from mongoengine import *




class User(Document):
    email = StringField(required=True)
    first_name = StringField(max_length=50)
    last_name = StringField(max_length=50)

class WC(Document):
    country = StringField(max_length=100)
    city = StringField(max_length=100)
    note = FloatField(max_value=10,min_value=0)
    site = StringField(max_length=200)
    site_type = StringField(max_length=50) 
    text = StringField(max_length=200)
    photo_path = StringField(max_length=200)
    
# para crear un nuevo WC: WC.objects.create(country = 'asa')