from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_exempt
import json
from search.models import User, WC
from django.core import serializers
from django.forms.models import model_to_dict


def index(request):
    return render_to_response('index.html', locals())

@csrf_exempt
def search(request):
    if request.method == 'POST' and request.is_ajax():
        filter_string = request.POST.get("filter_string")
        print "FILTRO: ",filter_string
        # users = serializers.serialize("json",User.objects.filter(email__contains="@"))
        wcs = WC.objects(site__contains = filter_string)
        print wcs.to_json()
        return HttpResponse(wcs.to_json(), content_type="application/json")

    else :
        return render_to_response('index.html', locals())



# from yourproject.models import Poll, Choice

# poll = Poll.objects(question__contains="What").first()
# choice = Choice(choice_text="I'm at DjangoCon.fi", votes=23)
# poll.choices.append(choice)
# poll.save()

# print poll.question

# import mongoengine

# # ...

# user = authenticate(username=username, password=password)
# assert isinstance(user, mongoengine.django.auth.User)